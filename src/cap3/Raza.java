package cap3;

public class Raza {
	private String nomRaça;
	private GosMida mida;
	private int tempsVida;
	private boolean dominant;
	
	public Raza() {
		nomRaça = "Murcigalgo";
		mida= GosMida.PETIT;
		tempsVida = 12;
		dominant = true;
	}
	
	Raza(String nom ,GosMida gt, int t){
		this(nom,gt);
		this.tempsVida = t;
	}
	Raza(String nom, GosMida gm){
		nomRaça = nom;
		mida = gm;
		tempsVida = 10;
		dominant = false;
	}

	public String getNomRaça() {
		return nomRaça;
	}

	public void setNomRaça(String nomRaça) {
		this.nomRaça = nomRaça;
	}

	public GosMida getMida() {
		return mida;
	}

	public void setMida(GosMida mida) {
		this.mida = mida;
	}

	public int getTempsVida() {
		return tempsVida;
	}

	public void setTempsVida(int tempsVida) {
		this.tempsVida = tempsVida;
	}

	public boolean isDominant() {
		return dominant;
	}

	public void setDominant(boolean dominant) {
		this.dominant = dominant;
	}

	@Override
	public String toString() {
		return "Raza= " + nomRaça + ", Mida= " + mida + ", Temps de Vida= " + tempsVida + ", Es dominant?= " + dominant+"\n";
	}
	
	
}
