package cap3;

import java.util.Random;

public abstract class Animal {
	static int numAnimals = 0;
	public Random rd = new Random();
	private int edad;
	private String nom;
	private int fills;
	private AnimalSexe animalSexe;
	private AnimalEstat aEstat;

	public Animal() {
		edad = 0;
		nom = "SenseNome";
		fills = 0;
		animalSexe = rd.nextBoolean() ? AnimalSexe.M : AnimalSexe.F;
		aEstat = AnimalEstat.VIU;
	}

	public Animal(int edat, String nom, cap3.AnimalSexe sexe) {
		this();
		this.edad = edat;
		this.nom = nom;
		this.animalSexe = sexe;
		this.aEstat = AnimalEstat.VIU;
	}

	public Animal(String nom) {
		this();
		this.edad = 0;
		this.nom = nom;
		this.animalSexe = rd.nextBoolean() ? AnimalSexe.M : AnimalSexe.F;
		this.aEstat = AnimalEstat.VIU;
	}

	public void clonarAnimal(Animal a) {

		this.edad = a.edad;
		this.nom = a.nom;
		this.fills = a.fills;
		this.animalSexe = a.animalSexe;
		this.aEstat = a.aEstat;

	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getFills() {
		return fills;
	}

	public void setFills(int fills) {
		this.fills = fills;
	}

	public AnimalSexe getAnimalSexe() {
		return animalSexe;
	}

	public void setAnimalSexe(AnimalSexe animalSexe) {
		this.animalSexe = animalSexe;
	}

	public AnimalEstat getaEstat() {
		return aEstat;
	}

	public void setaEstat(AnimalEstat aEstat) {
		this.aEstat = aEstat;
	}

	public static int getNumAnimals() {
		return numAnimals;
	}

	public static int getCont() {
		return numAnimals;
	}

	public void incEdat() {
		if (this.aEstat == AnimalEstat.VIU) {
			this.edad = edad++;
		}
	}
	public void incFills() {
		if (this.aEstat == AnimalEstat.VIU) {
			this.fills = fills++;
		}
	}

	public AnimalSexe AnimalSexe() {
		Random rnd = new Random();
		if (rnd.nextInt(2) == 0)
			animalSexe = AnimalSexe.M;
		else
			animalSexe = AnimalSexe.M;
		return animalSexe;
	}

	public void so() {
		System.out.println("Hola hola!!");
	};

	public abstract Animal aparellar(Animal nuvi);

	protected boolean esPodenAparellar(Animal animal1, Animal animal2) {
		if (animal1.animalSexe == animal2.animalSexe)
			return false;
		if (animal1.aEstat == AnimalEstat.MORT || animal2.aEstat == AnimalEstat.MORT)
			return false;
		return true;
	}

	protected abstract Animal crearFill(Animal a);

	public abstract Animal saludar(Animal altre);

	public String visualitzar() {
		String cadena= "Edat: "+ edad + "\n" 
				+ "Nom: "+ nom + "\n" 
				+ "Fills: "+  fills + "\n" 
				+ "Sexe: "+ animalSexe + "\n"
				+ "Estat: " + aEstat + "\n"; 			
		return cadena;
	}

	@Override
	public String toString() {

		String cadena = "+----------------------+" + "\n" + "Edat: " + edad + "\n" + "Nom: " + nom + "\n" + "Fills: "
				+ fills + "\n" + "Sexe: " + animalSexe + "\n" + "Estat: " + aEstat + "\n" + "+----------------------+";
		return cadena;
	}

}
