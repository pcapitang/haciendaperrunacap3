package cap3;

import java.util.Random;

public class Gos extends Animal{
	private Raza raza;

	@Override
	public void so() {
		System.out.println("Guau, guau!");
	}

	Gos() {
		super();
		raza = null;
	}

	Gos(int edat, String nom, Raza raza, AnimalSexe sexe) {
		super(edat, nom, sexe);
		this.raza = raza;
	}
	
	Gos(int edat, String nom, int fills, AnimalSexe sexe) {
		super(edat, nom, sexe);
		raza = null;
	}
	
	public void clonarGos(Gos g) {
		
		if (g instanceof Gos) {
			
			super.clonarAnimal(g);
			raza = g.getRaza();
		}
	}

	Gos(String nom) {
		super(nom);
		raza = null;
	}

	Gos(Gos g) {
		this();
		clonarAnimal(g);
	}

	public void setRaza(Raza raza) {
		this.raza = raza;
	}

	public Raza getRaza() {
		return raza;
	}
	public Gos aparellar(Gos g) {
		Gos fill = null;

		if (esPodenAparellar(this, g)) 
			fill = new Gos();
			fill.setEdad(0);
			fill.setNom("Fill de " + this.getNom() + " i "+ g.getNom());
			fill.setRaza(this.getRaza());
			fill.setAnimalSexe(rd.nextBoolean() ? AnimalSexe.M : AnimalSexe.F);
			
		return fill;
	}
	
	public void incEdat() {
		if (super.getaEstat() == AnimalEstat.VIU) {
			super.incEdat();
		}
	}

	private static boolean esPodenAparellar(Gos g1, Gos g2) {
		Gos mare, pare;
	
		if (g1.getAnimalSexe() == g2.getAnimalSexe()) return false;
		
		if (g1.getaEstat() != AnimalEstat.VIU || g2.getaEstat() != AnimalEstat.VIU) return false;
		
		if ((g1.getEdad() < 2 || g1.getEdad() > 10) || (g2.getEdad() < 2 && g2.getEdad() > 10)) return false;
			
		if (g1.getAnimalSexe() == AnimalSexe.F) {
				mare = g1;
				pare = g2;
		}
		else {
				mare = g2;
				pare = g1;
		}
				
		if (mare.getFills() >= 3) return false;
		
		if (mare.raza == null || pare.raza == null) return true;
		
		if (raçaPossible(mare.getRaza(), pare.getRaza()) )	return true;
		return false;

	}
	private static boolean raçaPossible(Raza raçam, Raza raçap) {
		boolean sino;
		
		switch (raçam.getMida()) {
		 	case PETIT: sino = raçap.getMida() == GosMida.PETIT;
		 				break;
		 	case MITJA: sino = raçap.getMida() == GosMida.PETIT || raçap.getMida() == GosMida.MITJA;
		 			    break;
		 	default: sino = true;
		}
		return sino;
	}
	
	@Override
	public String visualitzar() {

		String cadena = "GOS\n" + super.toString();

		if (raza != null) {
			cadena += "Raza: " + raza.getNomRaça() + "\n";
		} else {
			cadena += "Raza: DESCONEGUDA" + "\n";
		}
		
		return cadena;
	}


	@Override
	public String toString() {
		String cadena = "GOS\n" + super.toString();
		if (raza != null)
			cadena += raza.toString();
		else
			cadena += "Raza desconeguda\n";

		return (cadena);
	}

	@Override
	public Animal aparellar(Animal g) {
		Gos gosFill = null;
		if (g instanceof Gos == false)
			return gosFill;
		if (this.getaEstat()!=AnimalEstat.VIU || g.getaEstat()!=AnimalEstat.VIU)
			return gosFill;
		if (this.getAnimalSexe() == g.getAnimalSexe()) 
			return gosFill;
		
		gosFill = new Gos();
		gosFill.setNom("Fill de: " + this.getNom() + " / " + g.getNom());
		gosFill.setRaza(this.getRaza());
		
		this.setFills(this.getFills()+1);
		g.setFills(g.getFills()+1);
		
		return gosFill;
	}
	
	public void generarGos(int quantitatGenerar) {
		
		Gos gosGenerat = null;
		
		for (int i = 0; i < 5; i++) {	
			gosGenerat = new Gos();
			gosGenerat.setNom("gosGenerat" + (i+1));
			gosGenerat.setEdad(rd.nextInt(5));
			gosGenerat.incEdat();
			gosGenerat.visualitzar();
			System.out.println("Es porten creats " + Animal.getCont() + " animals." + "\n----------------------------");
		}
		
	}
	@Override
	protected boolean esPodenAparellar(Animal animal1, Animal animal2) {
		if (animal1.getAnimalSexe() == animal2.getAnimalSexe()) return false;
		
		if (animal1.getaEstat() == AnimalEstat.MORT || animal2.getaEstat() == AnimalEstat.MORT) return false;
		
		if (animal1 instanceof Gos == false || animal2 instanceof Gos == false) return false;
		
		return true;
	}

	@Override
	public Animal saludar(Animal altre) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Animal crearFill(Animal a) {
		// TODO Auto-generated method stub
		return null;
	}

	public static String quantitatGossos() {
		// TODO Auto-generated method stub
		return null;
	}



}