package cap3;

import java.util.ArrayList;
import java.util.Random;

public class Granja {
	private ArrayList<Animal> animals;
	private int numAnimals = 0;
	private int topAnimals = 100;
	static Random r = new Random ();


	public Granja() {
		animals = new ArrayList<Animal>();
		numAnimals = 0;
		topAnimals = 100;
	}

	public Granja(int top) {
		topAnimals = 100;
		if (top >= 1 && top <= 100) {
			topAnimals = top;
		}
		animals = new ArrayList<Animal>();
		numAnimals = 0;
	}

	public int getNumAnimals() {
		return numAnimals;
	}

	public int getTopAnimals() {
		return topAnimals;
	}

	public int afegir(Animal g) {
		if (numAnimals >= topAnimals) 
			return -1;
		
		animals.add(g);
		return ++numAnimals;
	}

	@Override
	public String toString() {
		String res;
		
		res = "\nGossos de la granja\n\n";
		for (int i = 0; i < numAnimals; i++)
			res = res + "\n" +  animals.get(i).toString();
		return res;
	}
	
	public void visualitzar() {
		System.out.println(this.toString());
	}
	
	public void visualitzarVius() {
		for (int i = 0; i < numAnimals; i++) {
			if (animals.get(i).getaEstat() == AnimalEstat.VIU) {
				animals.get(i).visualitzar();
			}
		}
	}
	
	
	public Animal obtenirGos(int pos) {
		if (pos >= this.getNumAnimals()) return null;
		else
			return this.animals.get(pos);
	}
	
	public void generarGranja (int ocupacio) {
		Random rnd = new Random();
		/*int limit = this.getTopAnimals();
		int qRaça;
		Raza raçaGos;
		int qedat;
		int resposta;
		int qSexe;
		
		Raza r1 = new Raza("Pastor",GosMida.GRAN, 20);
		Raza r2 = new Raza("Pug",GosMida.PETIT, 24);
		Raza r3 = new Raza("Caniche",GosMida.PETIT, 25);
		Raza r4 = new Raza("Bichon",GosMida.PETIT, 18);	
		
		for(int i=0;i<ocupacio;i++) {
			
			Gos g = new Gos();
			qRaça=rnd.nextInt(5);
			switch(qRaça) {
				case 1: 
					raçaGos = r1;
					break;
				case 2: 
					raçaGos = r2;
					break;
				case 3: 
					raçaGos = r3;
					break;
				case 4: 
					raçaGos = r4;
					break;
				default: 
					raçaGos = null;
			}			
			g.setRaza(raçaGos);
			
			qedat = rnd.nextInt(10);
			g.setEdad(qedat);
			
			g.setNom("Gos " + (i+1));
			
			qSexe = rnd.nextInt(2);
			if (qSexe==0)
				g.setAnimalSexe(AnimalSexe.M);
			else
				g.setAnimalSexe(AnimalSexe.F);
			
			resposta = this.afegir (g);
			if (resposta == -1) {
				System.out.println("Error: no s'ha pogut afegir " + g.getNom());
			}
		}*/
		int animalBoolean;
		int tipusGansoBoolean;

		for (int i = 0; i < topAnimals; i++) {

			animalBoolean = rnd.nextInt(2);

			switch (animalBoolean) {

			case 0:
				afegir(new Gos(rnd.nextInt(20), "Gos" + (i + 1), rnd.nextInt(5), rnd.nextBoolean() ? AnimalSexe.M : AnimalSexe.F));
				break;

			case 1:

				tipusGansoBoolean = rnd.nextInt(3);

				switch (tipusGansoBoolean) {
				case 0:
					afegir(new Ganso(rnd.nextInt(6),"Ganso" + (i + 1), TipusGanso.AGRESSIU, rnd.nextBoolean() ? AnimalSexe.M : AnimalSexe.F));
					break;

				case 1:
					afegir(new Ganso(rnd.nextInt(6), "Ganso" + (i + 1), TipusGanso.DESCONEGUT, rnd.nextBoolean() ? AnimalSexe.M : AnimalSexe.F));
					break;

				case 2:
					afegir(new Ganso(rnd.nextInt(6), "Ganso" + (i + 1), TipusGanso.DOMESTIC, rnd.nextBoolean() ? AnimalSexe.M : AnimalSexe.F));
					break;
				}

				break;
			}
		}

	}

	public int getnumAnimals() {
		return numAnimals;
	}	
	public Animal obtenirAnimal(int posicio) {

		if (posicio > animals.size()) {
			return null;
		} else {
			return animals.get(posicio - 1);
		}
	}

}
