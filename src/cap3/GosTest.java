package cap3;

public class GosTest {
	public static void main(String[] args) {
		Gos Batman = new Gos(5,"El señor de la noche",10, AnimalSexe.M);
		Batman.visualitzar();
		
		//Copia de el perro 1
		/*Gos PerroCopia= new Gos(Batman);
		PerroCopia.visualitzar();*/

		//Clonar el perro 1
		/*Gos PerroClon=new Gos();
		PerroClon.clonar(Batman);
		PerroClon.visualitzar();*/
		
		//Perro por defecto
		/*Gos Defecte= new Gos();
		Defecte.visualitzar();*/

		Raza r1 = new Raza ("Hammster", GosMida.PETIT,14);

		Gos teemo = new Gos(14,"Teemo", 7, AnimalSexe.M);		
		teemo.setRaza(r1);
		teemo.setaEstat(AnimalEstat.VIU);
		System.out.println(teemo);
		
		System.out.println("Teemo fa un any ... què passarà?\n");
		teemo.setEdad(teemo.getEdad()+1);
		System.out.println(teemo);
		
		System.out.println("Finalment: " + Gos.quantitatGossos() + " gossos");

	}


}