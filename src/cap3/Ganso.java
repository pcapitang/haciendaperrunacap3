package cap3;

public class Ganso extends Animal {
	private static int maxEdad = 6;
	private TipusGanso tipus;
	
	Ganso() {
		super();
		maxEdad = 6;
		tipus = TipusGanso.DESCONEGUT;
	}

	Ganso(int edat, String nom, TipusGanso tipus, AnimalSexe sexe) {
		super(edat, nom, sexe);
		this.tipus = tipus;
	}

	Ganso(String n) {
		super(n);
		tipus = TipusGanso.DESCONEGUT;
	}

	Ganso(Ganso g) {
		this();
		clonar(g);
	}

	public void setTipus(TipusGanso tipus) {
		this.tipus = tipus;
	}

	public TipusGanso getTipus() {
		return tipus;
	}

	@Override
	public void so() {
		System.out.println("Cuac Cuac");
	}

	public void clonar(Animal g) {
		if (g instanceof Ganso) {
			super.clonarAnimal(g);
			tipus = ((Ganso)g).getTipus();
		}
	}

	public int getEdatTope() {
		return Ganso.maxEdad;
	}
	
	@Override
	public String visualitzar() {
		String cadena = "GANSO\n" + super.toString();
		if (tipus != null) {
			cadena += "Tipus: " + getTipus() + "\n";
		} else {
			cadena += "Tipus: Desconegut!!" + "\n";
		}

		return cadena;

	}

	public String toString() {
		String cadena = "GANSO ";
		
		cadena += super.toString();
		cadena += "Tipus de Ganso" + this.getTipus();

		return (cadena);
	}

	@Override
	public void incEdat() {
		if (getaEstat() == AnimalEstat.VIU) {
			this.setEdad(this.getEdad()+1);
			actualitzarEstat();
		}
	}

	private void actualitzarEstat() {

		switch (this.getTipus()) {
			case AGRESSIU:
				if (this.getEdad() >= maxEdad - 1)
					this.setaEstat(AnimalEstat.MORT);
				break;
				
			case DOMESTIC:
				if (this.getEdad() >= maxEdad + 1)
					this.setaEstat(AnimalEstat.MORT);
				break;
				
			case DESCONEGUT:
				if (this.getEdad() >= maxEdad)
					this.setaEstat(AnimalEstat.MORT);
				break;
		}
	}

	public Animal aparellar(Animal a) {
		Ganso fill = null;
		int qSexe;
		if (a instanceof Ganso == false)
			return fill;

		if (this.getaEstat() != AnimalEstat.VIU || a.getaEstat() != AnimalEstat.VIU)
			return fill;

		if (this.getAnimalSexe() == a.getAnimalSexe())
			return fill;

		fill = new Ganso();
		fill.setEdad(0);
		fill.setNom("Fill de " + this.getNom() + "/" + a.getNom());
		fill.setTipus(this.getTipus());

		qSexe = (int) (Math.random() * 2);
		if (qSexe == 0)
			fill.setAnimalSexe(AnimalSexe.M);
		else
			fill.setAnimalSexe(AnimalSexe.F);
		
		this.incFills();
		a.incFills();

		return fill;
	}
	
	@Override
	public Animal saludar(Animal a) {
		Animal mort = null;

		if (a instanceof Gos) {
			Gos g = (Gos)a;
			if (this.getTipus() == TipusGanso.AGRESSIU && g.getEdad() < 2) {			
				g.setaEstat(AnimalEstat.MORT);
				mort =  g;
			}
			
			if (this.getTipus() != TipusGanso.AGRESSIU && g.getAnimalSexe() == AnimalSexe.F && g.getRaza()!=null) {
				
			}
			if (this.getTipus() != TipusGanso.AGRESSIU && g.getAnimalSexe() == AnimalSexe.F) {
				this.setaEstat(AnimalEstat.MORT);
				mort = this;
			}			
		}
		
		return mort;
	}


	@Override
	protected Animal crearFill(Animal a) {
		// TODO Auto-generated method stub
		return null;
	}

}
